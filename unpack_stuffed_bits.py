#!/usr/bin/python

import getopt
import sys

def checkForBitStuffing (bits, length):

    if len(bits) < length:
        raise Exception ('length of expected unstuffed bits should be less than total number of bits')
    unstuffedBits = [None] * length
    index = indexj = count = 0
    prev = None
    while indexj < length:

        if count >= 4:
            index +=1
            count = 0
            prev = None

        if bits[index] == prev:
            count += 1
        else:
            count = 0

        unstuffedBits[indexj] = bits[index]
        prev = bits[index]
        indexj += 1
        index += 1
     
    return ''.join(unstuffedBits)

def initialize(opts, arg, outputToFile):
    binaryInput = -1

    for opt, arg in opts:
        if "--rawin" == opt:
            binaryInput = arg
    return binaryInput

def main(argv):
    try:
        opts, args = getopt.getopt(argv,"o:s:", ["rawin="])
        binaryInput = initialize(opts, args, False)

    except getopt.GetoptError:
        print('identify_ecu.py --rawin <binary input>')
        sys.exit(2)

    unstuffedBits = checkForBitStuffing (binaryInput, 83)
    dataBits = unstuffedBits[19:]
    highWord = dataBits[0:32]

    print ("messageBits  : " + binaryInput)
    print ("unstuffedBits: " + unstuffedBits)
    print ("dataBits     : " + dataBits)
    print ("highWord     : " + highWord)
    print ("dec highWord : " + str (int (highWord, 2)))

if __name__ == "__main__":

    main(sys.argv[1:])