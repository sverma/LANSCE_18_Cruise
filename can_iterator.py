#!/usr/bin/python

import getopt
import math
import numpy
import pandas as pd
from pathlib import Path
import pprint
# from scipy.stats import skew, kurtosis
# from scipy.fftpack import fft
# from sklearn.cluster import KMeans
# from sklearn.model_selection import train_test_split
import struct
import sys
import time as clock

startingThreshold = 33000 #90
threshold = float("inf")
bitWidth = float("inf")
thresholdCeil = float("inf")
groups = {}
IdDescription = {
    256: "PLANT_CMD_ResetController",
    304: "PLANT_INFO_SimulationTime",
    306: "PLANT_INFO_VehicleSpeed",
    512: "CTRL_STATUS_Wakeup",
    528: "CTRL_INFO_TorqueCommand",
    529: "CTRL_INFO_VehSpd_ECHO",
    544: "CTRL_STATUS_ResetByPlant",
    560: "CONFIG_PARAMS"
    }

# Source: https://stackoverflow.com/a/1035456
# Alternative: https://stackoverflow.com/a/1035456
def getChunk(file, chunksize=2):
    while True:
        chunk = file.read(chunksize)
        if chunk:
            start = 0
            end = 2
            it = iter(chunk)
            while end < len(chunk):
                yield chunk[start:end]
                start = end
                end += 2
        else:
            break

def prettyPrint(item, location):
    print (location)
    # print (location + str(item))
    pprint.pprint(item)

def isValidStart(previousValues):
    global startingThreshold, threshold
    thresholdSuspassedNum = 0

    '''
    if the last pushed value is greater than threshold
    we don't need to look any further
    '''
    if threshold < previousValues[len(previousValues) - 1]:
        return True

    for value in previousValues:
        if (startingThreshold < value):
            thresholdSuspassedNum += 1

        if thresholdSuspassedNum > 3: return True

    return False

def initialize(opts, arg, outputToFile):
    global bitWidth, threshold, thresholdCeil, startingThreshold, groups
    seekTo = -1
    
    binDataFilePath = Path("")
    outputFilePath = Path("")
    outputFile = ""

    for opt, arg in opts:
        if "--rawin" == opt:
            binDataFilePath = Path(arg)
        elif '-s' == opt:
            # since data in binary file is unsigned short int, 2 bytes
            # seekTo = int(arg)
            seekTo = int(arg)*2
        elif '-o' == opt:
            outputFilePath = Path(arg)
            print ("**** Not in use**** outputFilePath: " + str(outputFilePath))
        elif '--threshold' == opt:
            threshold = int(arg)
        elif '--thresholdceil' == opt:
            thresholdCeil = int(arg)
        elif '--bitWidth' == opt:
            bitWidth = int(arg)

    if (not binDataFilePath.is_file()):
        raise Exception('invalid binary data file path')
    else:
        binDataFile = binDataFilePath.open('rb')

    if (outputFilePath == ""):
        raise Exception('invalid output file path')
    elif outputToFile:
        outputFile = outputFilePath.open('w')
    '''
    This stores what checkpoints each group will have
    ex: for group 01, it must have crossed threshold*2 
    and -threshold checkpoints, in that order
    -threshold indicates a falling edge, while threshold
    indicates a rising edge
    '''
    # TODO: update comment above with thresholdCeil
    groups = {
        "00":[thresholdCeil],
        "01":[(thresholdCeil), -threshold],
        "10":[threshold, thresholdCeil]
        }

    return binDataFile, seekTo, outputFile

def fastForwardTo(file, location):
    time  = 0
    if (location > 0 and file.seekable()):
        seekResult = file.seek(location)
        if (seekResult != location):
            raise Exception ("Seek not successful")
        time = location/2

    return time, file

def getBitPair(checkpoints):
#def getBitPair(checkpoints, bitIndex):
    global groups
    ret = ""

    if len(checkpoints) > 0:
        if (groups["01"] == checkpoints):   # dominant bit followed by a falling edge
            ret = "01"
        elif (groups["00"] == checkpoints): # two domminant bits
            ret = "00"
        elif (groups["10"] == checkpoints): # recessive bit followed by a rising edge
            ret = "10"
    else:
        #if (bitIndex > 0):
        ret = "11"

    # print ("group: " + ret)

    return ret

def getNextCheckpoint(value, checkpoints):
    global threshold, thresholdCeil
    ret = None

    if thresholdCeil <= value:
        if thresholdCeil not in checkpoints:
            ret = thresholdCeil
    elif threshold <= value:
        if thresholdCeil in checkpoints:
            if -threshold not in checkpoints \
                and threshold not in checkpoints:
                ret = -threshold
        else:
            if threshold not in checkpoints:
                ret = threshold
    return ret

def isEndOfFrame(bits):
    if (90 > len(bits)):
        return False

    ret = False
    counter = 0
    for item in reversed(bits):
        if counter >= 7:
            ret = True
            break
        if "1" == item:
            counter += 1
        else:
            break

    return ret

# Expects: SOF - Arbitration Field - ID Ext. Bit
def isExtendedFrame(messageBits):
    if 14 > len(messageBits):
        return False

    ret = False
    if "1" == messageBits[13]:
    # if 14 == len(messageBits) and "1" == messageBits[13]:
        ret = True
    return ret

def checkForBitStuffing (bits, length):

    if len(bits) < length:
        raise Exception ('length of expected unstuffed bits should be less than total number of bits')
    unstuffedBits = [None] * length
    index = indexj = count = 0
    prev = None
    while indexj < length:

        if count >= 4:
            index +=1
            count = 0
            prev = None

        if bits[index] == prev:
            count += 1
        else:
            count = 0

        unstuffedBits[indexj] = bits[index]
        prev = bits[index]
        indexj += 1
        index += 1
     
    return ''.join(unstuffedBits)

def extractData(messageBits):
    # extract 27 bits
    unstuffedBits = checkForBitStuffing(messageBits, 83)
    dataBits = unstuffedBits[19:]
    highWord = dataBits[0:32]
    lowWord = dataBits[32:]
    print ("Unstuffed bits: ", str(unstuffedBits))
    print ("Data bits: ", str(dataBits))
    print ("High word: ", str(int(highWord,2)))
    print ("Low word: ", str(int(lowWord,2)))


def parseID(messageBits):
    messageID = []
    messageID.append(checkForBitStuffing(messageBits, 14))
    # print ("messageID: " + str(messageID) + " len: " + str(len(messageID[0])))
    messageID[0] = (messageID[0])[1:]

    isFrameExtended = isExtendedFrame(messageID[0])
    #print ("isFrameExtended: " + str(isFrameExtended))
    if (isFrameExtended):
        messageID.append(checkForBitStuffing(messageBits[15:], 18))
        idBits = messageID[0] + messageID[1]
    else:
        # Extract first 11 bits
        idBits = (messageID[0])[0:11] # TODO: fix this in 499 repo

    if 11 <= len(idBits):
        return int(idBits,2)
    return -1

def main(argv):
    global bitWidth, threshold, startingThreshold

    try:
        opts, args = getopt.getopt(argv,"o:s:", ["rawin=","threshold=","thresholdceil=", "bitWidth="])
        binDataFile, seekTo, outputFile = initialize(opts, args, False)

    except getopt.GetoptError:
        print('identify_ecu.py --rawin <inputFile> -o <outputFile> -s <seekIndex> --threshold --thresholdceil --bitWidth')
        sys.exit(2)

    print ("File name: ", str(binDataFile.name))
    print ("threshold: " + str(threshold) + " -- bitWidth: " + str(bitWidth))

    time, binDataFile = fastForwardTo(binDataFile, seekTo)

    checkpoints = []
    startTime = stopTime = 0

    samples = {} # stores data for unclassifed group

    #bitIndex = 0
    messageBits = ""
    messageValues = {"00":[], "01":[], "10":[]}
    tmpMessageValues = {1:[],2:[]}
    startOfMessage = False
    endOfMessage = True
    mesageInfo = ""
    tmp = []
    sinceLastOutput = 0
    start = clock.time()
    singleRead = 2*100000
    tmpValues = []
    for b in getChunk(binDataFile, singleRead):

        time += 1
        uint16Value = convertToProperValue(b)

        isEnd = isEndOfFrame(messageBits)

        if startOfMessage and isEnd:
            # TODO: get unstuffed bits here
            messageId = parseID(messageBits)
            if -1 == messageId:
                raise Exception('invalid mesage ID')

            mesageInfo += " Stop Time: " + str(time)
            sinceLastOutput += 1

            # No need to store the values
            #if messageId in samples:
            #    for key, value in messageValues.items():
            #            samples[messageId][key].extend(value)
            #else:
            #    samples[messageId] = messageValues

            tmp.append(mesageInfo + "[" + str(messageId)+"] ")

            # print ("*************************")
            if (messageId not in IdDescription):
                print ("[" + str(messageId) + "] " + mesageInfo)
            else:
                if messageId == 304: # for better readability
                    print ("\n")
                print ("[" + IdDescription[messageId] + "] " + mesageInfo)
            # print ("*************************\n")
            print ("message bits: " + str(messageBits))
            extractData(messageBits)

            del messageBits
            messageBits = ""

            messageValues = {"00":[], "01":[], "10":[]}

            if not endOfMessage and startOfMessage:
                '''
                we want to shift start of the next bit from mid point,
                what we were doing initially, back to the inflection point
                '''
                startTime = stopTime + int(bitWidth/2)
                stopTime = calcualteIntervalPoint(startTime)
                endOfMessage = True
                startOfMessage = False
            else:
                startTime = calcualteIntervalPoint(stopTime)
                stopTime = calcualteIntervalPoint(startTime)

        if not startOfMessage:
            tmpCheckpoint = getNextCheckpoint(uint16Value, checkpoints)

            if (threshold == tmpCheckpoint):
                startOfMessage = True
                endOfMessage = False

                # here we shift back to sampling half of each bit
                startTime = time + int(bitWidth/2)
                stopTime = calcualteIntervalPoint(startTime)
                del mesageInfo
                mesageInfo = " Start Time: " + str(startTime)

                #print ("Start Time: " + str(startTime) + " Stop Time: " + str(stopTime))

        if startOfMessage:

            if time == stopTime:
                # print ("-----------------------------------\n")
                smoothedMean1 = numpy.mean(tmpMessageValues[1])
                # print ("Mean (1): ", str(smoothedMean1))

                smoothedMean2 = numpy.mean(tmpMessageValues[2])
                # print ("Mean (2): ", str(smoothedMean2))
                
                if smoothedMean1 > threshold and smoothedMean2 > threshold:
                    # signal is high
                    bitPair = '00'
                elif smoothedMean1 > threshold and smoothedMean2 < threshold:
                    # falling edge
                    bitPair = '01'
                elif smoothedMean1 < threshold and smoothedMean2 > threshold:
                    # rising edge
                    bitPair = '10'
                elif smoothedMean1 < threshold and smoothedMean2 < threshold:
                    # signal is low
                    bitPair = '11'

                # print ("bitPair: " + bitPair)
                if "" != bitPair:
                    messageBits += bitPair

                    tmpMessageValues[1].clear()
                    tmpMessageValues[2].clear()

                    startTime = calcualteIntervalPoint(stopTime)
                    stopTime = calcualteIntervalPoint(startTime)
                    bitPair = ""

            elif time >= startTime:
                if time - startTime <= 100:
                    tmpMessageValues[1].append(uint16Value)
                elif time - startTime >= 100:
                    tmpMessageValues[2].append(uint16Value)

    end = clock.time()
    print("Run time is: "+str(end - start))

def calcualteIntervalPoint(time):
    global bitWidth
    return time + bitWidth

def convertToProperValue(bit):
    unsignedShort = struct.unpack('H', bit) # 16 bytes
    return unsignedShort[0]

if __name__ == "__main__":

    # for testing
    # sys.argv.append('--rawin')
    # sys.argv.append('2018-10-17-1145--100MSPS--2V--kp634ki6kd2sp11nominal1--channel-B')
    # sys.argv.append('2018-10-05-1519--100MSPS--2V--kp634ki6kd2sp11experiment51--channel-B')
    sys.argv.append('--threshold')
    sys.argv.append('36500')

    # The iterator uses this to detect falling edge
    sys.argv.append('--thresholdceil')
    sys.argv.append('39110')
    
    sys.argv.append('--bitWidth')
    sys.argv.append('200')

    main(sys.argv[1:])
