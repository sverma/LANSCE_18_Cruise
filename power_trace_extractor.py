#!/usr/bin/python

import getopt
import math
from pathlib import Path
import pprint
import struct
import sys
import time as clock

# Source: https://stackoverflow.com/a/1035456
# Alternative: https://stackoverflow.com/a/1035456
def getChunk(file, chunksize=2):
    while True:
        chunk = file.read(chunksize)
        if chunk:
            start = 0
            end = 2
            it = iter(chunk)
            while end < len(chunk):
                yield chunk[start:end]
                start = end
                end += 2
        else:
            break

def prettyPrint(item, location):
    print (location)
    # print (location + str(item))
    pprint.pprint(item)

def initialize(opts, arg, outputToFile):
    seekTo = startTime = stopTime = -1
    
    binDataFilePath = outputFilePath = ""

    for opt, arg in opts:
        if "--rawin" == opt:
            binDataFilePath = Path(arg)
        elif '-s' == opt:
            # since data in binary file is unsigned short int, 2 bytes
            # seekTo = int(arg)
            seekTo = int(arg)*2
        elif '--startTime' == opt:
            startTime = int(arg)
        elif '--stopTime' == opt:
            stopTime = int(arg)
        elif '-o' == opt:
            outputFilePath = Path(arg)

    if (not binDataFilePath.is_file()):
        raise Exception('invalid binary data file path')
    else:
        binDataFile = binDataFilePath.open('rb')

    return binDataFile, startTime, stopTime, seekTo, outputFilePath.open('w+b')

def fastForwardTo(file, location):
    time  = 0
    if (location > 0 and file.seekable()):
        seekResult = file.seek(location)
        if (seekResult != location):
            raise Exception ("Seek not successful")
        time = location/2

    return time, file

def main(argv):

    seekTo = startTime = stopTime = -1
    try:
        opts, args = getopt.getopt(argv,"o:s:", ["rawin=","startTime=","stopTime="])
        binDataFile, startTime, stopTime, seekTo, outputFile = initialize(opts, args, False)

    except getopt.GetoptError:
        print('identify_ecu.py --rawin <inputFile> --startTime <start of sampling time> --stopTime <stop of sampling time> -o <outputFile> -s <seekIndex> --threshold --thresholdceil --bitWidth')
        sys.exit(2)     

    print ("File name: ", str(binDataFile.name))
    print ("Output file: ", str(outputFile.name))
    print ("startTime: ", str(startTime), " stopTime: ", str(stopTime))
    # print ("100 ms before startTime: " + str(startTime + 10000000)) # 10M samples in 100 ms before startTime
    startTime -= 10000000
    print ("100ms before and after startTime: ", str(startTime), " stopTime: ", str(stopTime))

    startTime
    time, binDataFile = fastForwardTo(binDataFile, seekTo)

    start = clock.time()
    singleRead = 2*100000
    for b in getChunk(binDataFile, singleRead):

        time += 1
        uint16Value = convertToProperValue(b)

        if startTime <= time:
            # print (str(uint16Value), end = ', ')
            outputFile.write(b)
            # print ("File read pointer location: " + str(binDataFile.tell()))
        if stopTime <= time or startTime < 0:
            outputFile.close()
            break


    end = clock.time()
    print("\nRun time is: "+str(end - start))

def calcualteIntervalPoint(time):
    global bitWidth
    return time + bitWidth

def convertToProperValue(bit):
    unsignedShort = struct.unpack('H', bit) # 16 bytes
    return unsignedShort[0]

if __name__ == "__main__":

    # for testing
    # sys.argv.append('--rawin')
    # sys.argv.append('2018-10-17-1145--100MSPS--2V--kp634ki6kd2sp11nominal1--channel-B')
    # sys.argv.append('2018-10-05-1519--100MSPS--2V--kp634ki6kd2sp11experiment51--channel-B')

    main(sys.argv[1:])
